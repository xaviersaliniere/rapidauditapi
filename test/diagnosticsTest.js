var supertest = require("supertest");
var should = require("should");

var server = supertest.agent("http://localhost:3000");

describe('Diagnostics', function() {

    describe('Add', function () {
        it('add diagnostic', function () {
            var diagnostic = {
                nomProprio: 'Sridar',
                date: '2016-03-31',
                diagnostic: 'Encore un diagnostic de test!'
            };

            server
                .post('/diagnostics/add?key=12345')
                .send(diagnostic)
                .expect("Content-type",/json/)
                .expect(200)
                .end(function(err,res){
                    res.status.should.equal(200);
                    res.body.error.should.equal(false);
                    res.body.data.should.equal(30);
                    done();
                });
        });
    });

    describe('Put', function () {
        it('modify diagnostic', function () {
            var diagnostic = {
                nomProprio: 'Thomas',
                date: '2016-03-02',
                diagnostic: 'Encore un diagnostic de test!'
            };

            server
                .put('/diagnostics/id/573b890fbab448981e7dd1b5?key=12345')
                .send(diagnostic)
                .expect("Content-type",/json/)
                .expect(200)
                .end(function(err,res){
                    res.status.should.equal(200);
                    res.body.error.should.equal(false);
                    res.body.data.should.equal(30);
                    done();
                });
        });
    });

    describe('Delete', function () {
        it('delete diagnostic', function () {
            server
                .delete('/diagnostics/id/all?key=12345')
                .expect(200)
                .end(function(err,res){
                    res.status.should.equal(200);
                    res.body.error.should.equal(false);
                    res.body.data.should.equal(30);
                    done();
                });
        });
    });

});