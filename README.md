# Informations #

### Tables MongoDB ###

* Table "visites"
* Table "diagnostics"


# Répartition des tâches #

### Listes des tâches ###

* **1-** Consultation des collections (visites & diagnostics), en fonction de la date. Ex: /visites/2016-03-31 ✔
* **2-** Obtenir les détais d'une visite (en fonction du nom et de la date) Ex: /visites/2016-03-31/Leroy ✔
* **3-** Obtenir un diagnostic (en fonction du nom et de la date) Ex: /diagnostics/2016-03-31/Leroy ✔
* **4-** Ajout de diagnostics ✔
* **5-** Modifier un diagnostics (sélection par rapport au nom du proprio et la date) ✔
* **6-** Supprimer un diagnostic (sélection par rapport au nom du proprio et la date) ✔

### Qui fait quoi ? ###

* Thomas -> **1**
* Sridar -> **2 & 3**
* Xavier -> **4 & 5 & 6**