var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    var db = req.db;
    var collection = db.get('visites');
    collection.find({},{},function(e,docs){
        if(e)
            res.send(e);
        res.json(docs);
    });
});

router.get('/id/:id', function(req, res, next) {
    var db = req.db;
    var collection = db.get('visites');
    var  id = req.param('id', "id");
    collection.find({"_id":id},{},function(e,docs){
        if(e)
            res.send(e);
        res.json(docs);
    });
});

router.get('/nom/:nomProprio', function(req, res, next) {
    var db = req.db;
    var collection = db.get('visites');
    var  nomProprio = req.param('nomProprio', "nomProprio");
    collection.find({"nomProprio":nomProprio},{},function(e,docs){
        if(e)
            res.send(e);
        res.json(docs);
    });
});

router.get('/date/:date', function(req, res, next) {
    var db = req.db;
    var collection = db.get('visites');
    var  date = req.param('date', "date");
    collection.find({"date":date},{},function(e,docs){
        if(e)
            res.send(e);
        res.json(docs);
    });
});

router.get('/dn/:date/:nomProprio', function(req, res, next) {
    var db = req.db;
    var collection = db.get('visites');
    var  date = req.param('date', "date");
    var  nomProprio = req.param('nomProprio', "nomProprio");
    //  collection.find({"date":date},{},function(e,docs){

    collection.find({"date":date,"nomProprio":nomProprio},{},function(e,docs){
        if(e)
            res.send(e);
        res.json(docs);
    });
    //  });
});

module.exports = router;
