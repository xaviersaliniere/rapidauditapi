var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    var db = req.db;
    var collection = db.get('diagnostics');
    collection.find({},{},function(e,docs){
        if(e)
            res.send(e);
        res.json(docs);
    });
});

router.post('/add', function(req, res) {
    var db = req.db;
    var collection = db.get('diagnostics');
    collection.insert(req.body, function(err, result){
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );
    });
});

router.route('/id/:id')

    .get(function(req, res) {
        var collection = req.db.get('diagnostics');
        var  id = req.param('id', "id");
        collection.find({"_id":id},{},function(e,docs){
            if(e)
                res.send(e);
            res.json(docs);
        });
    })

    .put(function(req) {
        var collection = req.db.get('diagnostics');

        collection.findAndModify(
            { _id: req.params.id },
            { $set: {
                "nomProprio": req.body.nomProprio,
                "date": req.body.date,
                "diagnostic": req.body.diagnostic
            }});
    })

    .delete(function(req, res) {
        var collection = req.db.get('diagnostics');

        if(req.params.id === 'all') var filter = {};
        else var filter = {_id: req.params.id};

        collection.remove(filter, function(e, docs) {
            if(e)
                res.send(e);
            res.json({message: 'Le diagnostic a correctement été supprimée.'});
        });
    });

router.get('/id/:id', function(req, res, next) {
    var db = req.db;
    var collection = db.get('diagnostics');
    var  id = req.param('id', "id");
    collection.find({"_id":id},{},function(e,docs){
        if(e)
            res.send(e);
        res.json(docs);
    });
});

router.get('/nom/:nomProprio', function(req, res, next) {
    var db = req.db;
    var collection = db.get('diagnostics');
    var  nomProprio = req.param('nomProprio', "nomProprio");
    collection.find({"nomProprio":nomProprio},{},function(e,docs){
        if(e)
            res.send(e);
        res.json(docs);
    });
});

router.get('/date/:date', function(req, res, next) {
    var db = req.db;
    var collection = db.get('diagnostics');
    var  date = req.param('date', "date");
    collection.find({"date":date},{},function(e,docs){
        if(e)
            res.send(e);
        res.json(docs);
    });
});

router.get('/dn/:date/:nomProprio', function(req, res, next) {
    var db = req.db;
    var collection = db.get('diagnostics');
    var  date = req.param('date', "date");
    var  nomProprio = req.param('nomProprio', "nomProprio");
    //  collection.find({"date":date},{},function(e,docs){

    collection.find({"date":date,"nomProprio":nomProprio},{},function(e,docs){
        if(e)
            res.send(e);
        res.json(docs);
    });
    //  });
});

module.exports = router;